/*
import { Schema } from "mongoose";
*/
var express = require("express");
var app = express();
var bodyparser = require("body-parser");
var mongoose = require("mongoose");


mongoose.connect("mongodb://localhost/yelp_camp");
app.use(bodyparser.urlencoded({extended:true}));
app.set("view engine","ejs");

//schema setup
var campgroundSchema= new mongoose.Schema({
    name:String,
    image:String,
    Description:String
});

var campground = mongoose.model("Campground",campgroundSchema);
/*campground.create({
    name : "salmon Creek",
    image:"https://farm3.staticflickr.com/2655/3738566424_180036be3f.jpg",
    Description:"this is the fuck hill if it"
}
,function(err,campground){
    if(err)
    {
        console.log(err);
    }else{
        console.log("new campground created......")
        console.log(campground);
    }
});*/
var campgrounds = [
    {name : "salmon Creek",image:"https://farm3.staticflickr.com/2655/3738566424_180036be3f.jpg"},
    {name : "Granite Hill",image:"https://farm7.staticflickr.com/6031/5892882006_192d4f4406.jpg" },
    {name : "salmon Creek",image:"https://farm3.staticflickr.com/2655/3738566424_180036be3f.jpg"},
    {name : "Granite Hill",image:"http://www.photosforclass.com/download/534238043" },
    {name : "salmon Creek",image:"https://farm7.staticflickr.com/6031/5892882006_192d4f4406.jpg"}

];

app.get("/",function(req,res){
    //res.render("Landing");
    res.render("landing");
});

app.get('/campgrounds',function(req,res){
   // res.render("Landing");

    campground.find({},(err,allCampgrounds)=>{
    if(err)
    {
        console.log(err);
    }else{
        res.render("index",{campgrounds:allCampgrounds});
    }
  });
});
app.post('/campgrounds',function(req,res){
var name = req.body.name;
var image = req.body.image;
var newCampgrounds = {name:name,image:image};
//campgrounds.push(newCampgrounds);
campground.create(newCampgrounds,(err,newlyCreated)=>{
    if(err)
    {
        console.log(err);

    }else{
        res.redirect('/campgrounds');
        console.log(newCampgrounds);
    }

});
});


app.get('/campgrounds/new',function(req,res){
res.render('new.ejs');
});

app.get('/campgrounds/:id',(req,res)=>{
//res.render('show.ejs');
campground.findById(req.param.id,(err,foundcampground)=>{
        if(err){
            console.log(err);
        }else{
            res.render('show',{Campground:foundcampground});
        }
    })

});

var port =process.env.PORT || 8000;
app.listen(port, function () {
    console.log('Example app listening on port 8000!')
 });


 //203 the number of stop
 /*notes
 to get the data from the form we use request.body-parser
 */
